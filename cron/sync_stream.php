<?php include_once('/var/www/inc/.php');


$ram_dir = '/var/www/html/stream/';

$res = shell_exec(" ps aux | grep -v grep | grep -v 'sh -c' | grep ffmpeg | grep '{$ram_dir}' | rev | awk {'print $1'} | rev | cut -d/ -f6 ");
$ps_s = [];

foreach( explode("\n", $res) as $r )
	if( $r = trim($r, "\r\n\t ") )
		$ps_s[] = $r;

if( $json = curl(SIGNAL_POINT.'/api/feed/codec/assigned/') ){

	$rw_s = json_decode($json, true);

	if( sizeof($rw_s) ){
		foreach( $rw_s as $id => $source ){
			
			// echo "$id => $source\n";

			$source_dir = $ram_dir.$id;
			$m3u = $source_dir."/master.m3u8";
			$log = "{$source_dir}/log.txt";


			if(! file_exists($source_dir) )
				mkdir($source_dir);

			if( file_exists($m3u) and ( filemtime($m3u) > date('U') - 60 ) and in_array($id, $ps_s) ){
				echo "{$id}: already running\n";

			} else {

				// shell_exec(" ps aux | grep -v grep | grep -v 'sh -c' | grep ffmpeg | grep '{$m3u}' | awk {'print \$2'} | xargs kill ");

				echo "{$id} started {$source}\n";

				$cmd = " ffmpeg -y -nostdin -hide_banner -loglevel warning -err_detect ignore_err -user_agent \"VLC/2.2.4 LibVLC/2.2.4\" -start_at_zero -copyts -vsync 0 -correct_ts_overflow 0 -avoid_negative_ts disabled -max_interleave_delta 0 -probesize 7000000 -analyzeduration 7000000 -i '{$source}' -vcodec copy -scodec copy -acodec copy -individual_header_trailer 0 -f hls -hls_time 4 -hls_list_size 3 -hls_flags delete_segments -hls_segment_filename {$source_dir}/_%d.ts {$source_dir}/master.m3u8 ";

				shell_exec($cmd." >{$log} 2>{$log} & ");


			}

		}
	}


	if( sizeof($ps_s) ){
		$id_s_in_db = array_keys($rw_s);
		foreach( $ps_s as $id ){
			if(! in_array($id, $id_s_in_db) ){
				
				echo "{$id}: killing\n";
				$source_dir = $ram_dir.$id;
				echo shell_exec(" ps aux | grep -v grep | grep ffmpeg | grep '{$source_dir}/' | awk {'print \$2'} ");
				echo shell_exec(" ps aux | grep -v grep | grep ffmpeg | grep '{$source_dir}/' | awk {'print \$2'} | xargs kill ");
				shell_exec(" rm -rf {$source_dir} ");

			}
		}
	}

	echo "DONE\n";

}




