
cd /var
rm -rf www
git clone https://gitlab.com/i3-public/codec.git
mv codec www

cp /var/www/conf/.env-sample /var/www/.env

apt -y install ffmpeg

wget -qO- https://gitlab.com/i3-public/codec/-/raw/main/conf/cron.txt > /var/spool/cron/crontabs/root

wget https://gitlab.com/i3-public/net-usage/-/raw/master/README.md -O ~/net-usage-installer.sh
bash ~/net-usage-installer.sh 3913 skipserver
