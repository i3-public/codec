
# main port: 3913
# wget -qO- https://gitlab.com/i3-public/codec/-/raw/main/README.txt | bash

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash

wget -O dockerfile https://gitlab.com/i3-public/conf/-/raw/main/nginx-on-docker/conf/dockerfile
sed -i 's,#INJECT,RUN wget -qO- https://gitlab.com/i3-public/codec/-/raw/main/conf/patch.sh | sh,g' dockerfile

docker rm -f codec
docker rmi codec-image

docker build -t codec-image -f dockerfile .
docker run -t -d --restart unless-stopped --name codec --mount type=tmpfs,destination=/var/www/html/stream -p 3922:22 -p 3913:80 codec-image

# wget -qO- https://gitlab.com/i3-public/net-usage/-/raw/master/README.txt | bash -s 3913 skipserver
